package com.vvvtimes.springbootvelocity.Controller;

import com.vvvtimes.springbootvelocity.Model.Email;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1/email")
public class EmailRestController {


    @Autowired
    private VelocityEngine velocityEngine;

    @RequestMapping("version")
    @ResponseStatus(HttpStatus.OK)
    public String version() {
        return "[OK] Welcome to withdraw Restful version 1.0";
    }

    @RequestMapping(value = "hello", method = {RequestMethod.POST, RequestMethod.GET})
    public String hello() throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("hello", "test velocity");
        String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "templates/hello.vm", "UTF-8", model);
        System.out.println(text);
        return text;
    }

    @RequestMapping(value = "template", method = {RequestMethod.POST, RequestMethod.GET})
    public String template() throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
        Email email = new Email();
        email.setFrom("发送者");
        email.setTo("接收者");
        email.setSubject("主题");
        email.setText("内容");
        email.setStatus(true);

        model.put("title", email.getSubject());
        model.put("body", email.getText());
        String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "templates/email.vm", "UTF-8", model);
        System.out.println(text);
        return text;
    }
}
